#define UNIX
//#define WDOS

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/wait.h>

// variables de entorno
int paCount = 30;
int paCurrent = 0;
char *params[30];


int arCount = 30;
int arCurrent = 0;
char *args[30];


int m_substr(const char *str, int inicio, int count, char *dest)
{
    if (strlen(dest) < count)
        return false;

    int i, j = 0;
    for (i = inicio; i < count + inicio; i++)
    {
        if (i >= strlen(str))
            break;
        dest[j++] = str[i];
    }

    dest[j] = '\0';
    return true;
}

int split(const char *chr, const char *str)
{
        char list[100];
        char tmp[100];
        int i = 0, j = 0, k;
        for (k = 0; k < strlen(str); k++)
        {
                int fl = m_substr(str, k, strlen(chr), tmp);
                if (fl)
                        if (!strcmp(chr, tmp))
                        {
                                list[j++]='\0';
                                params[i] = (char*)malloc((j ) * sizeof(char));
                                strncpy(params[i], list, j);
                                i++;
                                j = 0;
                                k = k + strlen(chr) - 1;
                        }
                        else list[j++] = str[k];
        }

        list[j++] = '\0';
        params[i] = (char*)malloc(j * sizeof(char));
        strncpy(params[i], list, j);

        return i + 1;
}

void buildArgs(int inicio, int *rCount)
{
    int i;
   // for (i = 0; i < rCount[0]; i++)
     //   free(args[i]);

    rCount[0] = 0;

    for (i = inicio; i < paCurrent; i++)
    {
        if (i >= paCurrent) break;

        args[rCount[0]] = (char*)malloc(strlen(params[i]) * sizeof(char));
        strcpy(args[rCount[0]], params[i]);
        rCount[0]++;
    }
    if (rCount[0] == 0)
        args[rCount[0]++] = "\0";

    args[rCount[0]] = NULL;
}

void Execute(char *task, int input, int output)
{
    paCurrent = split(" ", task);
    buildArgs(0, &arCurrent);

    pid_t pID = fork();
    if (pID == 0) //estoy en el hijo
    {
       dup2(input, 0);
       dup2(output, 1);
       execvp(params[0], args);
    }
    else waitpid(pID, NULL, 0);
}

void copyArry(char *result[], char *source[], int sLength)
{
    int i;
    for (i = 0; i < sLength; i++)
    {
        //free(result[i]);
        result[i] = malloc(strlen(source[i]) * sizeof(char));
        strcpy(result[i], source[i]);
    }

}

//input y output son los descriptores de ficheros de donde esta la entrada y la salida del proceso
void ParseLine(char *line, int input, int output)
{
    if (split(">>", line) > 1)
    {
        int fd = open(params[1], O_CREAT|O_WRONLY|O_APPEND, 0644);

        ParseLine(params[0], input, fd);

        close(fd);
    }
    else if (split(">", line) > 1)
    {
        int fd = open(params[1], O_CREAT|O_WRONLY|O_TRUNC, 0644);

        ParseLine(params[0], input, fd);

        close(fd);
    }
    else if (split("<", line) > 1)
    {
        int fd = open(params[1], O_RDONLY, 0644);

        int pipefds[2];
        pipe(pipefds); //creando un pipe

        // poniendo todas las lineas del fichero en el pipe
        int size = 512;
        char *buff = (char*)malloc(size * sizeof(char));

        int readed;
        while ((readed = read(fd, buff, size)) > 0)
            write(pipefds[1], buff, readed);

        free(buff);
        close(fd);

        ParseLine(params[0], pipefds[0], output);

        close(pipefds[0]);
        close(pipefds[1]);
    }
    else
    {
        //solamente hay | � espacios
        paCurrent = split("|", line);
        if (paCurrent > 1) //hay almenos un pipe, se ejecutaran almenos dos comandos
        {
            char *tmp[30];
            int x = paCurrent;
            copyArry(tmp, params, paCurrent);

            int pipefds[2];
            pipe(pipefds);

            ParseLine(params[0], input, pipefds[1]);
            paCurrent = x;
            copyArry(params, tmp, x);

            int i;
            for (i = 1; i < paCurrent - 1; i++)
            {
                ParseLine(params[i], pipefds[0], pipefds[1]);
                paCurrent = x;
                copyArry(params, tmp, x);
            }

            ParseLine(params[paCurrent - 1], pipefds[0], output);
            paCurrent = x;
            copyArry(params, tmp, x);

            close(pipefds[0]);
            close(pipefds[1]);
        }
        else Execute(line, input, output); //no hay pipes, es decir: no hay caracteres especiales, solamente espacios
    }
}

void MainLoop()
{
    printf("%s\n", "Shell developed by Nicolas Perez (nikos)");

    while (true)
    {
        printf("%s", "N$:");

        char input[255];
        gets(input);

        if (strcmp("exit", input) == 0)
            break;
        if (strcmp("", input) == 0)
            continue;

        ParseLine(input, 0, 1);
    }
}

int main(int argc, char *argv[])
{
    MainLoop();

    return 0;
}

